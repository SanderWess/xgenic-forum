"use strict";

import express, { request } from 'express';        // Viewport
import mysql from 'mysql';            // DB driver
import bodyParser from 'body-parser'; // HTML-JSON handler
import jwt, { decode } from 'jsonwebtoken';       // Cookies
import bcrypt from 'bcryptjs';        // Password hashing
import cookieParser from 'cookie-parser';


//import ejs from 'ejs';
//import path from 'path';

const app = express();
const PORT = 8081;

app.listen(PORT, () => {
  console.log('Running...');
});


app.use(express.static("public"));  // Use public folder in server
app.set('view engine', 'ejs');

// Parse JSON bodies (as sent by API clients)
app.use(express.json());

// Parse URL-encoded bodies (as sent by HTML forms)
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cookieParser());

var db = mysql.createConnection({
  host: "db",
  user: "admin",
  password: "password",
  database: 'prog2053-proj'
});

db.connect(function (err) {
  if (err) {
    throw err;
  }
  console.log("Connected!");
});

app.get("/", function (req, res) {
  res.render("home");
});




app.get('/addPost1', function (req, res) {
  res.render('addPost1');
});

app.get("/login", function (req, res) {
  res.render("login");
});

app.get("/register", function (req, res) {
  res.render("register");
});

app.get('/submit', (req, res) => {
  res.render('submit')
});


//----------------INPUT


app.post("/register", function (req, res) {
  var email = req.body.email;
  var password = req.body.password;

  db.query('SELECT email FROM users WHERE email = ?', email, async (error, results) => {
      if(error) { //checks for email duplication
        console.log(error);
      }

     if (results.length > 0) {
      return res.render('register', {   //error if email in use
        message: 'That email is already in use!'
      })
      
      } else if(password !== password) {  //error if password mismatch
        return res.render('register', {
         message: 'Passwords do not match!'
        });
      }

      let hashedPassword = await bcrypt.hash(password, 8);
      console.log(hashedPassword);

      db.query('INSERT INTO users SET ?',
       {email: email, password: hashedPassword}, (error, results) => {
         if (error) {
           console.log(error);

         } else {
           console.log(results);

          return res.render('register', {
            message: 'User registered!'
           });
         }
       })
       
  });

});

app.post("/login", async (req, res) => {
  try {
    var email = req.body.email;
    var password = req.body.password;

    if (!email || !password) {
      return res.render('login', {
        message: 'Please provide an email and password'
      })
    }

    db.query('SELECT * FROM users WHERE email = ?', email, async(error, results) => {
      console.log(results);
      // error for password missmatch or missing
      if(!results || !(await bcrypt.compare(password, results[0].password))) {
        res.status(401).render('login', {
          message: 'Email or password is incorrect'
        })
      } // We ask for result[0] as only one result is supposed to be there
      else {
        // Creating cookies
        const id = results[0].uid;

        const token = jwt.sign({uid: id}, 'tokenPassWord', {
          expiresIn: '90d'
        });

        console.log("The token is: " + token);

        const cookieOptions = {
          expires: new Date(
            Date.now() + 90 * 24 * 60 * 60 * 1000
          ),
          httpOnly: true
        }

        res.cookie('myCookie', token, cookieOptions);
        res.redirect('/main');
      }
    })

  } catch {error} {
    console.log(error);
  }
});


app.post('/getUsers', function (req, res) {
  db.query('', function (err, result) {
    if (err) {
      res.status(400).send('Error in database operation.');
    } else {
      res.writeHead(200, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify(result));
      //console.log("Result: " + res);
    }
  });
});







//skal hente inn alle postsa inn i main

app.get('/main', function(req, res) {
  
  var dataSQL;
  var user;
  var title;
  var content;
  var email;
  var userType;
  var userResultResult;

  //Henter inn riktig data fra tabellene posts og users
  db.query('SELECT posts.pid, posts.user, title, content, users.email AS email, userType AS userType FROM posts JOIN users ON posts.user=users.uid ORDER BY posts.pid DESC',
      {user: user, title: title, content: content, email: email, userType: userType}, (error, result) => {
        if(error){
          console.log("errorShow");
        }
        else{
          console.log("PostShown");  //skriver inn i konsollen om den greide å hente result
          userResultResult=result;

          // Rendrer main og setter dataSQL variablen til outputten av sql-queryen (3d array)
          res.render('main',{
            dataSQL: userResultResult
          });
        };
      });
});

app.post("/addPost1", function (req, res) {

  var kjeks = req.cookies.myCookie;

  if (kjeks != "" && kjeks != null){
    var title = req.body.title;     //henter innholdet av "title" fra main.ejs som er det man putter i input fieldet.
    var content = req.body.content; //henter innholdet av "content" fra main.ejs
    
    // Verifier innlogingen med jwt sammen med passord og henter ut uid
    var user = jwt.verify(req.cookies.myCookie, "tokenPassWord").uid;
    
    // Inserter bare dersom man er logget inn aka .uid == number
    if (typeof user == 'number'){
      if (title != "" && content != ""){
        db.query("INSERT INTO posts SET ?",     //setter inn variabelen user inn i user dataen i mysql, eller content.
          {user: user, title: title, content: content}, (error, res) => {
          
          if(error){
            console.log("errorzz");   //om noe ble scuffed skal det skrives ut errorzz i konsollen
          }
          
          else{
          console.log("postCrt"); //om alt gikk planlagt skal det skrives ut postCrt i konsollen
          };
        });
      }
    }
    // Redirect hvis token er feil
    else{
      res.redirect('/login');
    }
  }
  // Redirect hvis man ikke er logget inn / cookien ikke er satt
  else{
    res.redirect('/login');
  }
  // Redirecter til main, slik at det nye innlegget også kan bli rendera
  res.redirect('/main');
});
