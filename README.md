# Prosjekt - PROG2053

## Project status
Finished.

## Name - XGENIC - FORUMS

## Description (Overview)
This app is a forum website that uses MYSQL to put and store data. 

## Feutures
- You can register accounts by using an email and a password.
- You can login to see post history and also create new posts.

## Installation notes: 
- Install nodeJs
- Run "npm i" in both the server and client folders of the project.    
- While in the root folder of the project: "docker-compose up -d" - this will take a long time the first time you do it.
- Are you getting an error after doing "docker-compose up -d", saying "[...] Filesharing has been cancelled"? 
-> Go to Docker for Windows app (or similar) -> Settings -> Resources -> File sharing -> Add all your drives (or play around with figuring out what exactly you need).
- Are you getting an error saying "npm ERR! ch() never called"? 
-> Delete "package-lock.json" from the client directory, then build the client again using "docker-compose build client"

Want to reset your containers and volumes fully? 
- "docker system prune -a --volumes"

Want to get in to a container for some reason? 
- "docker-compose exec <containername> bash" 

## Group members:     
Sander Langfeldt Wessel
Luka Mikami Salyga
Kasper Emmerhoff
Adiran Robert Gindaoanu
Mikael Panes Gundersen

   
## Setup: 
- docker-compose up -d   

The project runs on localhost:8080   

## Usage

- To use this application, run the servers in docker and then go to localhost:8081 on any browser.
- To register an account press the register button.
- To login, press the login button and use the information used to register the account.
- after logging in it will direct you to the post site where you can see the most recent posts, and you can also create a new post by filling in the input field "title:" and "content:".

